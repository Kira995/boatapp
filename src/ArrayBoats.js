//CLASS BOAT & Init ARRAY OF BOATS
import Boat from './Boat.js';
import './App';
import'./Tornado.jpg';
import'./Riva.jpg';
import'./Gianetti.jpg';
import'./Apreamare.jpg';
import'./Sunseeker.jpg';
import'./Mito.jpg';
import'./Jeranto.jpeg';
 
const ArrayBoats = [];
ArrayBoats.length = 7;

LoadData(0,"/static/media/Tornado.45cdda66.jpg",'Tornado','The Tornado are the result of a technological revolution design, combining techniques of the best traditional Italian nautical materials and innovative engineering solutions, to build boats for excellence quality and performance.','12 pax','13,50 mt');
LoadData(1,"/static/media/Riva.46598794.jpg",'Rivarama','The hull profile is characterized by a double-curved stratified glass window at the level of the cabin. The new model has been designed with sinuous and streamlined lines, which make it seductive and sporty at the first glance. ','12 pax','13 mt');
LoadData(2,"/static/media/Gianetti.c238d05a.jpg",'Gianetti 45','Is a real masterpiece of stylistic solution and advanced technologies that make it a yacht which places at the highest positions in the sport cruisers','12 pax','13,60 mt');
LoadData(3,"/static/media/Apreamare.c2e2dc0f.jpg",'Apreamare','Apreamare has not only produced boats, but it has produced authentic jewels. Highly skilled carpenters and shipwrights work closely together on each model of the standard range as well as the custom built boats.','12pax','11,56 mt');
LoadData(4,"/static/media/Sunseeker.30638b1f.jpg",'Sunseeker','This Fast and sleek yacht is perfect for big groups or families looking to spend an amazing day out on the crystal waters of Golfo di Napoli, Sardinia and Ventotene and Ponza. Let our crew take care of you whilst you relax on one of the many sun beds.','12 pax','22,90 mt');
LoadData(5,"/static/media/Mito.1b5761b6.jpg",'Mito','Sober and elegant design, which never sets. Every detail is designed to be used to the best. Large spaces on board, large easily accessible lockers, bow and stern sundecks to accommodate many people on board, ease of changing the layout of spaces with simple movements.','18 pax','13,50 mt');
LoadData(6,"/static/media/Jeranto.c0c562aa.jpeg",'Jeranto','The gozzo Jeranto 9 is traditional in appearance, with a modern high and round stern, which is comparable to that of gliding boats. Its name Jeranto refers the homonymous bay which is located south of Punta Campanella, opposite the island of Capri.','12 pax','9 mt');

function LoadData(i,img,nam,des,cap,len){
    ArrayBoats[i] = new Boat();
    ArrayBoats[i].image = img;
    ArrayBoats[i].name = nam;
    ArrayBoats[i].descr = des;
    ArrayBoats[i].capacity = cap; 
    ArrayBoats[i].length = len;
    ArrayBoats[i].calendar = [];
    console.log(ArrayBoats);
}
    
   
export default ArrayBoats;