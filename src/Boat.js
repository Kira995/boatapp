import React  from'react';


class Boat extends React.Component{
    Boat(props){
    this.image = '';
    this.name = '';
    this.key = this.name.toString();
    this.capacity = '';
    this.length = '' ;
    this.descr = '';
    this.calendar = [];
    this.render();
    this.calendarender();
    }
    calendarender(){
        let p='';
        for(let i=0; i<this.calendar.length; i++)
        p=p+'  '+this.calendar[i];
        console.log(this.calendar);
        return p;

    }
    render(){
        return (
 <div className="Boat">
    <img className='foto' src={this.image}/>
    <div className='contents'>
        <h1 className='Title'>{this.name}</h1>
        <div className="text">{'Descrizione: '+this.descr}<br/>{'Portata: '+this.capacity}<br/>{'Lunghezza: '+this.length}</div>
        </div>
        <input className='data' type='date'></input>
        <button className='Add' onClick={(e)=>{
            console.log(e.target.parentElement.children[2].valueAsDate);
            if(e.target.parentElement.children[2].value=="")
            alert('data non impostata...');
            else{
            this.calendar.indexOf(e.target.parentElement.children[2].value) != -1 ? alert('Data non disponibile..') : this.calendar.push(e.target.parentElement.children[2].value);
            console.log(this.calendar);
            e.target.parentElement.children[5].click();
            console.log(this.calendar);
            e.target.parentElement.children[2].value=null;
            }}}>+</button>
        <button className='Rm' onClick={(e)=>{
            if(e.target.parentElement.children[2].value=="")
            alert('data non impostata...');
            else{
            this.calendar.indexOf(e.target.parentElement.children[2].value) != -1 ? this.calendar.splice(this.calendar.indexOf(e.target.parentElement.children[2].value),1) : alert('Data non presente..');
            e.target.parentElement.children[5].click();
            console.log(this.calendar);
            e.target.parentElement.children[2].value=null;
        }}}>-</button>
        <div className='renderprenotations' onClick={(e)=>{
            e.target.textContent = this.calendarender();
        }}></div>
        </div>
        );
    }
  }
  export default Boat;