import './App.css';
import ArrayBoats from './ArrayBoats.js';

//FUNCTIONS

function App() {
  return (
    <div className="App">
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[0].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Tornado</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[1].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Riva</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[2].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Gianetti 45</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[3].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Apreamare</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[4].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Sunseeker</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[5].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Mito</p1></div>
     <div className="cont"><div className="icon"><div className="subboat" onClick={Expand}></div>{ArrayBoats[6].render()}<button className='resizebtn' onClick={Resize}>Resize to icon</button></div><p1>Jeranto</p1></div>
    </div>
  );
}

function Expand(event){
  let div = event.currentTarget;
  let boat = event.currentTarget.parentElement.children[1];
  let image = boat.firstChild;
  let btn = event.currentTarget.parentElement.children[2];
  let icon = event.currentTarget.parentElement;
  div.style.display = 'none';
  setTimeout(function(){
  image.style = 'height:350px;width:250px;';
  boat.style = 'display: flex;';
  btn.style = 'display:block;';
  },400);
  icon.style = 'height:1000px;width:290px;';
}

function Resize(event){
  let clickdiv = event.currentTarget.parentElement.children[0];
  let boat =  event.currentTarget.parentElement.children[1];
  let btn = event.currentTarget.parentElement.children[2];
  let icon = event.currentTarget.parentElement;
  icon.style = 'height:60px;width:60px;';
  btn.style = 'width:0;height:0;';
  boat.style = 'width:0;height:0;';
  clickdiv.style = 'display:block;';
}

export default App;
